package com.egosventures.car360capturesample.application;

import android.app.Application;
import android.util.Log;

import com.egosventures.stable360.capture.Stable360CaptureFramework;
import com.egosventures.stable360.core.Constants;
import com.egosventures.stable360.core.Stable360Framework;
import com.egosventures.stable360.core.Stable360FrameworksHelper;

public class CaptureSampleApplication extends Application {

    private final static String TAG = "SampleApplication";

    private static final String USER_EMAIL = "YOUR_EMAIL";
    private static final String USER_PASSWORD = "YOUR_PASSWORD";

    @Override
    public void onCreate() {

        // Call super:
        super.onCreate();

        Stable360CaptureFramework.getInstance(this).init(USER_EMAIL, USER_PASSWORD);

        Stable360FrameworksHelper.addInitListener(new Stable360Framework.InitListener() {

            @Override
            public void onInitDone() {

                Log.i(TAG, "onInitDone");
            }

            @Override
            public void onInitError(Stable360Framework.Stable360FrameworkError stable360FrameworkError) {

                Log.e(TAG, "onInitError " + stable360FrameworkError.name());
            }
        });
    }
}
