package com.egosventures.car360capturesample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.egosventures.car360capturesample.R;
import com.egosventures.stable360.capture.fragments.LocalViewerFragment;
import com.egosventures.stable360.core.Stable360Framework;
import com.egosventures.stable360.core.utils.IconPosition;

public class ViewerActivity extends FragmentActivity implements LocalViewerFragment.LocalViewerFragmentLoadingListener {

    // ---------------------------------------------------------------------------------------------
    // region Public attributes

    public final static String TAG = "ViewerActivity";
    public final static String EXTRA_SPIN_ID = "EXTRA_SPIN_ID";

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Private attributes

    private BroadcastReceiver fragmentMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            int event = intent.getIntExtra(LocalViewerFragment.EXTRA_FRAGMENT_EVENT, -1);

            switch (event) {

                case LocalViewerFragment.EVENT_NULL_USER: {

                    Log.e(TAG, "EVENT_NULL_USER");
                    break;
                }

                case LocalViewerFragment.EVENT_FRAMEWORK_INIT_ERROR: {

                    // Get the error:
                    Stable360Framework.Stable360FrameworkError error = (Stable360Framework.Stable360FrameworkError) intent.getSerializableExtra(LocalViewerFragment.EXTRA_FRAMEWORK_INIT_ERROR);
                    Log.e(TAG, "EVENT_FRAMEWORK_INIT_ERROR: " + error.name());
                    break;
                }
            }
        }
    };

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region Lifecycle

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_viewer);

        if (getIntent() != null && getIntent().getStringExtra(EXTRA_SPIN_ID) != null) {
            String spinId = getIntent().getStringExtra(EXTRA_SPIN_ID);

            LocalViewerFragment viewerFragment = new LocalViewerFragment();
            Bundle bundle = new Bundle();

            // The spin id is a mandatory extra:
            bundle.putString(LocalViewerFragment.EXTRA_SPIN_ID, spinId);

            // You can customize the background color:
            bundle.putSerializable(LocalViewerFragment.EXTRA_BACKGROUND_COLOR, Color.RED);

            // You can customize the message displayed if the framework initialisation led to a FRAMEWORK_INIT_ERROR event:
            bundle.putBoolean(LocalViewerFragment.EXTRA_SHOW_FRAMEWORK_INIT_ERROR_MESSAGE, true);
            bundle.putString(LocalViewerFragment.EXTRA_FRAMEWORK_INIT_ERROR_MESSAGE, "Custom framework init error message");
            bundle.putInt(LocalViewerFragment.EXTRA_FRAMEWORK_INIT_ERROR_ICON, R.drawable.icon_arrow_cancel);
            bundle.putSerializable(LocalViewerFragment.EXTRA_FRAMEWORK_INIT_ERROR_ICON_POSITION, IconPosition.RIGHT);

            // You can customize the message displayed if the framework initialisation led to a NULL_USER event:
            bundle.putBoolean(LocalViewerFragment.EXTRA_SHOW_NULL_USER_MESSAGE, true);
            bundle.putString(LocalViewerFragment.EXTRA_NULL_USER_MESSAGE, "Custom null user message");
            bundle.putInt(LocalViewerFragment.EXTRA_NULL_USER_ICON, R.drawable.icon_arrow_cancel);
            bundle.putSerializable(LocalViewerFragment.EXTRA_NULL_USER_ICON_POSITION, IconPosition.BOTTOM);

            viewerFragment.setArguments(bundle);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.viewerFrameLayout, viewerFragment).commit();

            viewerFragment.setLoadingListener(this);
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(fragmentMessageReceiver, new IntentFilter(LocalViewerFragment.BROADCAST_FRAGMENT_MESSAGE));
    }

    @Override
    protected void onPause() {

        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fragmentMessageReceiver);
    }

    // endregion
    // ---------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------
    // region LocalViewerFragment.LocalViewerFragmentLoadingListener

    @Override
    public void onLoadingDone() {

        Log.i(TAG, "Local spin have successfully been loaded");
    }

    @Override
    public void onLoadingError(LocalViewerFragment.LoadingError loadingError) {

        Log.e(TAG, "An error occurred while loading the local spin: " + loadingError.name());
    }

    // endregion
    // ---------------------------------------------------------------------------------------------
}
